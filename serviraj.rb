#!/usr/bin/ruby
require 'em-websocket'
FILE = ARGV.first
$cs = []
$file=File.open(FILE)
$file.seek(0,IO::SEEK_END)
module Handler
  def file_modified
    new_input = $file.read
    $cs.each {|c| c.send new_input }
  end
end

EM.run do
  EM.watch_file(FILE, Handler)
  EM::WebSocket.run(:host => "0.0.0.0", :port => 8085) do |ws|
    ws.onopen do |handshake|
      $cs << ws
    end
    ws.onclose do
      $cs.delete ws
    end
  end
end

